@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Mahasiswa</div>
                <div class="card-body">
                <div class="col-6">
                    <ul class="list-group">
                        @foreach($mahasiswa as $mahasiswa)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{$mahasiswa->nama}}
                            <a href="/mahasiswa/{{ $mahasiswa->id }}" class="badge badge-info">Detail</a>
                        </li>
                        @endforeach
                    </ul>
                </div>


		@endsection