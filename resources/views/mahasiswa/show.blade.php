@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Detail Mahasiswa <a href="/mahasiswa" class="badge badge-warning">Kembali</a> </div>
                <div class="card-body">
                <table class="table table-bordered">
                    <thead class="bg-info">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Nim</th>
                        <th scope="col">email</th>
                        <th scope="col">Jurusan</th>
                        <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>{{$mahasiswa->id}}</td>
                        <td>{!! $mahasiswa->nama !!}</td>
                        <td>{!! $mahasiswa->nim !!}</td>
                        <td>{!! $mahasiswa->email !!}</td>
                        <td>{!! $mahasiswa->jurusan !!}</td>
                        <td>
                            <a href="" class="badge badge-success">Edit</a>
                            <a href="" class="badge badge-danger">Hapus</a>
                        </td>
                        </tr>
                    </tbody>
                </table>
		@endsection